from setuptools import setup, find_packages

long_description ='''openaps dose plugin
==============================
This package is a vendor plugin for openaps that recommends and applies doses
'''

requires = ['openapscontrib.predict', 'python-dateutil']

__version__ = None
exec(open('openapscontrib/dose/version.py').read())

setup(
    name='openapscontrib.dose',
    version=__version__,
    url='https://bitbucket.org/loudnate/openaps-dose',
    license='MIT',
    author='Nathan Racklyeft',
    author_email='loudnate+pypi@gmail.com',
    description='openaps dose plugin',
    long_description=long_description,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Topic :: Documentation',
        'Topic :: Utilities',
    ],
    platforms='any',
    packages=find_packages(exclude=['tests']),
    include_package_data=True,
    install_requires=requires,
    namespace_packages=['openapscontrib'],
    test_suite='tests'
)
