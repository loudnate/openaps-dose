"""
dose - tools for recommending and applying pump doses


"""
from .version import __version__

import argparse
from datetime import datetime
from datetime import timedelta
from dateutil.parser import parse
import json
import os

from openaps.uses.use import Use
from openaps.vendors.medtronic import MedtronicTask
from openaps.vendors.medtronic import use

from dose import calculate_doses


# set_config is needed by openaps for all vendors.
# set_config is used by `device add` commands so save any needed
# information.
# See the medtronic builtin module for an example of how to use this
# to save needed information to establish sessions (serial numbers,
# etc).
def set_config(args, device):
    # no special config
    return


# display_device allows our custom vendor implementation to include
# special information when displaying information about a device using
# our plugin as a vendor.
def display_device(device):
    # no special information needed to run
    return ''


# openaps calls get_uses to figure out how how to use a device using
# agp as a vendor.  Return a list of classes which inherit from Use,
# or are compatible with it:
def get_uses(device, config):
    return [recommend]


def _opt_date(timestamp):
    """Parses a date string if defined

    :param timestamp: The date string to parse
    :type timestamp: basestring
    :return: A datetime object if a timestamp was specified
    :rtype: datetime|NoneType
    """
    if timestamp:
        return parse(timestamp)


def _json_file(filename):
    return json.load(argparse.FileType('r')(filename))


def _opt_json_file(filename):
    """Parses a filename as JSON input if defined

    :param filename: The path to the file to parse
    :type filename: basestring
    :return: A decoded JSON object if a filename was specified
    :rtype: dict|list|NoneType
    """
    if filename:
        return _json_file(filename)


# noinspection PyPep8Naming
class recommend(Use):
    """Recommend a dosing change

    """
    @staticmethod
    def configure_app(app, parser):
        """Define command arguments.

        Only primitive types should be used here to allow for serialization and partial application
        in via openaps-report.
        """
        parser.add_argument(
            'predicted-glucose-without-basal',
            help='JSON-encoded glucose prediction data file, omitting any future basal changes, '
                 'created by openapscontrib.predict'
        )

        parser.add_argument(
            '--resolved-history',
            help='JSON-encoded history data file, resolved by openapscontrib.mmhistorytools'
        )

        parser.add_argument(
            '--settings',
            help='JSON-encoded pump settings file'
        )

        parser.add_argument(
            '--clock',
            help='JSON-encoded pump clock file'
        )

        parser.add_argument(
            '--bg-targets',
            help='JSON-encoded glucose targets schedule file'
        )

        parser.add_argument(
            '--insulin-sensitivities',
            help='JSON-encoded insulin sensitivities schedule file'
        )

        parser.add_argument(
            '--basal-profile',
            help='JSON-encoded basal rate schedule file'
        )

        parser.add_argument(
            '--allow-predictive-temp-below-range',
            action='store_true',
            help='Allow basal insulin to continue if glucose is below range but expected to rise'
        )

    def get_params(self, args):
        params = {key: args.__dict__.get(key) for key in (
            'predicted-glucose-without-basal',
            'resolved_history',
            'settings',
            'clock',
            'bg_targets',
            'insulin_sensitivities',
            'basal_profile'
        )}

        allow_predictive_temp_below_range = args.__dict__.get('allow_predictive_temp_below_range')

        if allow_predictive_temp_below_range:
            params['allow_predictive_temp_below_range'] = allow_predictive_temp_below_range

        return params

    @staticmethod
    def get_program(params):
        """Parses params into constructor arguments

        :param params:
        :type params: dict
        :return:
        :rtype: tuple(list, dict)
        """
        clock_file_time = datetime.fromtimestamp(os.path.getmtime(params.get('clock')))
        prediction_file_time = datetime.fromtimestamp(
            os.path.getmtime(params.get('predicted-glucose-without-basal'))
        )
        now = datetime.now()

        assert now - clock_file_time < timedelta(minutes=5), 'Clock data is more than 5 minutes old'
        assert now - prediction_file_time < timedelta(minutes=5), \
            'Prediction data is more than 5 minutes old'

        args = (
            _json_file(params.pop('predicted-glucose-without-basal')),
        )

        kwargs = {}

        if params.pop('allow_predictive_temp_below_range', None):
            kwargs['allow_predictive_temp_below_range'] = True

        kwargs.update({key: _json_file(f) for key, f in params.items()})

        return args, kwargs

    def main(self, args, app):
        args, kwargs = self.get_program(self.get_params(args))

        return calculate_doses(*args, **kwargs)


# noinspection PyPep8Naming
@use()
class deliver(MedtronicTask):
    """Apply a list of dosing changes

    """
    def configure_app(self, app, parser):
        """Define command arguments.

        Only primitive types should be used here to allow for serialization and partial application
        in via openaps-report.
        """
        parser.add_argument(
            'recommendation',
            help='JSON-encoded dosage recommendation data file'
        )

    def get_params(self, args):
        return dict(recommendation=args.recommendation)

    @staticmethod
    def get_program(params):
        """Parses params into constructor arguments

        :param params:
        :type params: dict
        :return:
        :rtype: tuple(list, dict)
        """
        recommendation_file_time = datetime.fromtimestamp(
            os.path.getmtime(params.get('recommendation'))
        )
        now = datetime.now()

        assert now - recommendation_file_time < timedelta(minutes=5), \
            'Recommendation data is more than 5 minutes old'

        args = (
            _json_file(params.pop('recommendation')),
        )

        return args, params

    def main(self, args, app):
        args, kwargs = self.get_program(self.get_params(args))

        return self.deliver_doses(*args, **kwargs)

    def deliver_doses(self, doses):
        output = []

        for program in doses:
            ptype = program.get('type')

            try:
                if ptype == 'TempBasal':
                    result = self.pump.model.set_temp_basal(**program)
                elif ptype == 'Bolus':
                    result = self.pump.model.bolus(**program)
                else:
                    result = {}
            except IndexError:
                pass
            else:
                program.update(timestamp=datetime.now(), **result)
            output.append(program)

        return output
